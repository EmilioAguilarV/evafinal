/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package root.services;

import cl.dao.PalabrasJpaController;
import cl.entities.Palabras;
import java.net.MalformedURLException;
import java.util.List;
import javax.net.ssl.HttpsURLConnection;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.PathParam;
import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.WebTarget;

/**
 *
 * @author Mclio
 */
@Path("/palabras")
public class PalabraRest {

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public Response listarTodo() {

        PalabrasJpaController dao = new PalabrasJpaController();
        List<Palabras> lista = dao.findPalabrasEntities();
        System.out.println("lista.size():" + lista.size());
        return Response.ok(200).entity(lista).build();

    }

    @GET
    @Path("/{idbuscar}")
    @Produces(MediaType.APPLICATION_JSON)
    public Response buscarId(@PathParam("idbuscar") String idbuscar) throws MalformedURLException {
        PalabrasJpaController dao = new PalabrasJpaController();
        Palabras sel = dao.findPalabras(idbuscar);
        return Response.ok(200).entity(sel).build();
        
        Client client = ClientBuilder.newClient();
        WebTarget myResource = client.target("https://od-api.oxforddictionaries.com/api/v2/entries/es/" + idbuscar);
        String app_id = "3d187c0c";
        String app_key = "1b2138915895629d22deed4aa918197b";
             HttpsURLConnection urlConnection = (HttpsURLConnection) url.openConnection();
                    urlConnection.setRequestProperty("app_id",app_id);
                    urlConnection.setRequestProperty("app_key",app_key);
        
        
    }

}
